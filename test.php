<?php
// php test.php test.txt +

if ($argc < 3) {
    echo 'Error: Specify parameters File and Operation (+, -, *, /)';
    exit;
} 

if (is_readable($argv[1])) {
    echo 'Error: File not found or not readable!';
    exit;
}

$validOperations = ['+', '-', '*', '/'];

if (!in_array($argv[2], $validOperations)) {
    echo 'Error: Not valid operation! Enter from the following values +, -, *, /';
    exit;
}

switch ($argv[2]) {
    case '+':
        $func = function(int $a, int $b) {
            return $a + $b;
        };
        break;
    case '-':
        $func = function(int $a, int $b) {
            return $a - $b;
        };
        break;
    case '*':
        $func = function(int $a, int $b) {
            return $a * $b;
        };
        break;
    case '/':
        $func = function(int $a, int $b) {
            if ($b == 0) {
                echo 'Error: Division by zero!';
            } else {
                return $a / $b;
            }
        };
        break;
}

$fileArr = file($argv[1]);

$negative = '';
$positive = '';

foreach ($fileArr as $line) {
    if (preg_match('/^(-?[1-9]+) (-?[1-9]+)\r?\n?$/', $line, $matches)) {
        $res = $func(intval($matches[1]), intval($matches[2]));

        if ($res < 0) {
            $negative .= $res . PHP_EOL;
        } else {
            $positive .= $res . PHP_EOL;
        }
    } else {
        echo 'Error: Bad file format!';
        exit;
    }
}

file_put_contents('positive.txt', $positive);
file_put_contents('negative.txt', $negative);