<?php
// php test2.php test.txt +

if ($argc < 3) {
    echo 'Error: Specify parameters File and Operation (+, -, *, /)';
    exit;
} 

if (is_readable($argv[1])) {
    echo 'Error: File not found or not readable!';
    exit;
}

$validOperations = ['+', '-', '*', '/'];

if (!in_array($argv[2], $validOperations)) {
    echo 'Error: Not valid operation!';
    exit;
}

$file = file_get_contents($argv[1]);

if (!preg_match('/^(-?[1-9]+ -?[1-9]+\r?\n?)+$/', $file)) {
    echo 'Error: Bad file format!';
    exit();
}

$file = str_replace(" ", $argv[2], $file);
$file = explode(PHP_EOL, $file);

$negative = '';
$positive = '';

foreach ($file as $line) {
    eval('$res = ' . $line . ';');

    if ($res < 0) {
        $negative .= $res . PHP_EOL;
    } else {
        $positive .= $res . PHP_EOL;
    }
}

file_put_contents('positive.txt', $positive);
file_put_contents('negative.txt', $negative);